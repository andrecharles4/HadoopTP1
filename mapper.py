import csv
import sys
import numpy as np
from collections import defaultdict

def map(key, value):
    dict = {}           # Create an empty dictionnary
    i=1
    for val in value:   # Go through all the values
        newval = str(key) + "," + val   # Create a string with "numofline, valueofthecell"
        dict[i]=newval                  # Add this to a dictionnary with the number of column as the key
        i+=1
    return dict                         # Return the dictionnary

