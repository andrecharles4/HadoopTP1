import csv
import sys
import numpy as np

def reduce(value):
    arr = []                # Create an empty array
    for key in value:       # Go through  the input to get the key (num of column)
        for val in value[key]:      # Get the value for this key
            val = val.split(',')    # Split to get the num of the line and the value separately
            arr.append(val[1])      # Add the value to the array

        # Create new dictionnary
    dict = {}
    dict[key] = arr             # Add the array to the dictionnary where the key remain the number of the column
    return dict                 # Retturn this dictionnary, which represent a line with the number of the line as the key

