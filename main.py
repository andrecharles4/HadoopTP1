import csv
import sys
import numpy as np


from mapper import map
from reducer import reduce

file = open('file.csv', 'r', encoding='utf-8-sig')
reader = csv.reader(file, skipinitialspace=True, quoting=csv.QUOTE_NONE, delimiter=' ', escapechar=' ')
characters = {}
i = 1
for row in reader:
    line = str(row)

        #  Remove unwanted characters in the string, such as "'" "[}" or ";;"
    line = line.replace('[', '')
    line = line.replace(']', '')
    line = line.replace("'", '')
    line = line.replace(';;', '')

        # Split the string when ',' to get alle the values of the line
    line = line.split(';')

        # Call the mapper function by sending e.g (1, ['a', 'b', 'c']) for the first line
    values = map(i, line)
    i +=1


# Then Shuffle and Sort part
    # Data that is returned by the mapper :
resultmap1 = {1: '1,a', 2: '1,b', 3: '1,c'}
resultmap2 = {1: '2,d', 2: '2,e', 3: '2,f'}
resultmap3 = {1: '3,g', 2: '3,h', 3: '3,i'}

# After shuffle and sort :
shufflesort1 = {1: ['1,a', '2,d','3,g']}
shufflesort2 = {2: ['1,b', '2,e','3,h']}
shufflesort3 = {3: ['1,c', '2,f','3,i']}



# Then call the reducer
    # Call the reducer for each of this datasets
firstline = reduce(shufflesort1)
secondline = reduce(shufflesort2)
thirdline = reduce(shufflesort3)

    # We now have all the new lines of the matrix
print(firstline)
print(secondline)
print(thirdline)



